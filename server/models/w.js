const fs = require('fs');
const data = require('./words.json')


const wordList = data.wordList
const checkPossibility = (options,str) => {
    return options.some(item => str.startsWith(item))
}
const words = (options) => wordList.filter(word =>checkPossibility(options,word)) 

module.exports = words 