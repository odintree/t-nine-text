const express= require('express') 
const bodyParse = require('body-parser') 
const showOptions = require('./models/o')
const predictWords = require('./models/w')

const app = express()
const port = process.env.PORT || 8080


app.use(bodyParse.json())

app.get('/', (req,res) => {
    res.send('text')
})



app.get('/api/getWords', (req,res) => {
    let theQuery = req.query.data
    const data = (query) =>{
    return (
        {
            options: showOptions(query),
            words: predictWords(showOptions(query))
        }
    )}    
    res.send(data(theQuery)) 

})






app.listen(port,() => {
    console.log(`server run on port number ${port}`)
})

