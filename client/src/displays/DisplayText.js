import React from "react";
import PropTypes from "prop-types";



const DisplayText = (props) => {
    console.log('props1112222',props)
    const DispShort = (props.show) ? 'none' : 'block' 
    const DispLong = (!props.show) ? 'none' : 'block' 
    const short = {
        display: DispShort
    }
    const long = {
        display: DispLong
    }
    const toDisplay = props.value.slice(0,10).join(', ')
    const toDisplayAll = props.value.join(', ')
    return (
      <div className={props.style}>
        <div style={short}>{props.title}: {toDisplay}</div>
        <div style={long}>{props.title}: {toDisplayAll}</div>
      </div>
    );
}


DisplayText.propTypes = {
  value: PropTypes.array,
};
export default DisplayText;
