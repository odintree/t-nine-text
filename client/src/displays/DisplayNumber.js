import React from "react";
import PropTypes from "prop-types";
import "../styles/Display.css";


const DisplayNumber = (props) => {
    return (
      <div className="component-display">
        <div>{props.value}</div>
      </div>
    );
}


DisplayNumber.propTypes = {
  value: PropTypes.string,
};
export default DisplayNumber;
