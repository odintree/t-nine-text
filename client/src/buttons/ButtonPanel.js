import Button from "./Button";
import React from "react";
import PropTypes from "prop-types";

import "../styles/ButtonPanel.css";

const ButtonPanel = (props) =>{
const handleClick = buttonName => {
    props.clickHandler(buttonName);
};

    return (
        <div className="component-button-panel">
            <div>
            <Button name="1"/>
            <Button name="2" clickHandler={handleClick} />
            <Button name="3" clickHandler={handleClick} />
            </div>
            <div>
            <Button name="4" clickHandler={handleClick} />
            <Button name="5" clickHandler={handleClick} />
            <Button name="6" clickHandler={handleClick} />
            </div>
            <div>
            <Button name="7" clickHandler={handleClick} />
            <Button name="8" clickHandler={handleClick} />
            <Button name="9" clickHandler={handleClick} />
            </div>  
            <div>
            <Button name="DEL" clickHandler={handleClick}/>
            </div>
        </div>
    )
}
ButtonPanel.propTypes = {
  clickHandler: PropTypes.func,
};
export default ButtonPanel;
