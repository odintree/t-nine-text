import React from "react";
import PropTypes from "prop-types";
import "../styles/Button.css";

class Button extends React.Component {
  handleClick = () => {
    this.props.clickHandler(this.props.name);
  };
  displayLetters = function (number) {
    switch(number){
      case '2': return 'abc'
      case '3': return 'def'
      case '4': return 'ghi'
      case '5': return 'jkl'
      case '6': return 'mno'
      case '7': return 'pqrs'
      case '8': return 'tuv'
      case '9': return 'wxyz'
      default: return ''
  }
  }
  render() {
    const className = [
      "component-button",
    ];
    return (
      <div className={className.join(" ").trim()}>
        <button onClick={this.handleClick}>
          <div>{this.props.name}</div>
          <span>{this.displayLetters(this.props.name)}</span>
        </button>
      </div>
    );
  }
}
Button.propTypes = {
  name: PropTypes.string,
  orange: PropTypes.bool,
  wide: PropTypes.bool,
  clickHandler: PropTypes.func,
};
export default Button;
