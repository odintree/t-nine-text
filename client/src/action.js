
const Action = {
    writeNumbers: (state,button) => {
        return {numbers: state.numbers + button}
    },
    deleteNumber: (state) => {
        return {numbers: state.numbers.slice(0,-1)}
    },
    writeWords: (state,words) => {
        console.log('words', words)
        return {words: words}
    },
    emptyWords: (state,empty) => {
        return {words: empty}
    },
    writeOptions: (state,options) => {
        return {options: options}
    },
    changeWordsView: (state) => {
        var value = (state.isAllWords) ? false : true
        return {isAllWords: value}
    },
    changeOptionView: (state,isAllOptions) => {
        var value = (state.isAllOptions) ? false : true
        return {isAllOptions: value}
    },
}

export default Action;
