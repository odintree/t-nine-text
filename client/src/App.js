import React from "react";
import DisplayNumber from "./displays/DisplayNumber";
import DisplayText from "./displays/DisplayText";
import ButtonPanel from "./buttons/ButtonPanel";
import Action from "./action"
import "./styles/App.css";
import axios from "axios"


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numbers: '',
      words: [],
      options:[],
      isAllWords: false,
      isAllOptions: false
    };
  }

  checkNumber = () => {
    if (!this.state.numbers) {
      console.log('thiahai',this.state)
      this.setState(Action.emptyWords(this.state,[]))
    }
  }
  checkWords = (numbers) =>  {
    axios.get('/api/getWords?data='
                  +numbers)
            .then(res => {
              console.log('res',res)
              this.setState(Action.writeWords(this.state, res.data.words))
              this.setState(Action.writeOptions(this.state, res.data.options))
              this.checkNumber()
              console.log('state', this.state)
            })
  }
  handleClick = buttonName => {
    if (buttonName === "DEL") {
      this.setState(Action.deleteNumber(this.state), () => {
        console.log('number',this.state.numbers)
        this.checkWords(this.state.numbers)
      })
    }
    else {
      this.setState(Action.writeNumbers(this.state, buttonName), () => {
        console.log('number',this.state.numbers)
        this.checkWords(this.state.numbers)
      })
    }
  };

  showWords = () => {
    this.setState(Action.changeWordsView(this.state))
  }
  showOptions = () => {
    this.setState(Action.changeOptionView(this.state))
  }


  render() {
    return (
      <div className="component-app">
        <ButtonPanel clickHandler={this.handleClick} />
        <DisplayNumber value={this.state.numbers || "Number Box"} />
        <button className="word-button" onClick={this.showWords}> (Show 10 / show ALL) PREDICTIONS</button>
        <button className="option-button" onClick={this.showOptions}> (Show 10 / show ALL) OPTIONS</button>
        <DisplayText value={this.state.words || "Prediction box"} 
                      show={this.state.isAllWords}
                      style='component-display-word'
                      title='REAL WORDS'     />
        <DisplayText value={this.state.options || "All possible options"} 
                      show={this.state.isAllOptions}
                      style='component-display-opt'  
                      title='Possible combinations'   />
      </div>
    );
  }
}
export default App;
